// This adjusts the options in a selection box based on what was chosen in a previous selection box

const characterRace = document.querySelector<HTMLSelectElement>('#race')!;
const characterSubrace = document.querySelector<HTMLSelectElement>('#subrace')!;

const races = [
    ["Arasha", ["Asai","Halfspawn", "Narmaata", "Raane", "Sen", "Svormar"]],
    ["Azaer" , ["Ajagar","Arasha", "Human", "Pureblood", "Nudar", "Ur"]],
    ["Human" , ["Aes Ardra","Halfspawn", "Magi", "Sister of the Dawn", "Wildkin", "Witch"]],
    ["Nudar" , ["Aman","Arali", "Aveen", "Halfspawn", "Lan", "Shana", "Vatari"]]
];

export function adjustSubraceOptions() {
    characterRace.addEventListener('change', () => {
        const raceValue = characterRace.options[characterRace.selectedIndex].text;
        let str = "";
        
        races.forEach((value) => {
            if (raceValue === value[0]) {
                str += "<option value=''>Select your subrace</option>";
                (<string[]>value[1]).forEach((rvalue) => {
                    str += "<option value='" + rvalue + "'>" + rvalue + "</option>"
                });
            }
        });

        if (str == "") {
            characterSubrace.innerHTML = "<option value=''>No subrace available</option>";
            // characterSubrace.disabled = true;
        }

        else {
            characterSubrace.innerHTML = str;
            // characterSubrace.disabled = false;
        }
    });
};


