// This determines what content is visible on the page

import { generateCharacterTable  } from "./characterTable"
import { exportCharacterJSON  } from "./exportCharacterJSON"
import { adjustSubraceOptions } from "./subraceState"
import { addCharacterToArray  } from "./characterObjectArray"

const createCharacterButton = document.querySelector<HTMLDivElement>('#create_character_button')!;
const createAnotherCharacterButton = document.querySelector<HTMLDivElement>('#create_another_character_button')!;
const backToTableButton = document.querySelector<HTMLDivElement>('#back_table_button')!;

const sideButtonIntro = document.querySelector<HTMLDivElement>('#side_bar_button_intro')!;
const sideButtonCreate = document.querySelector<HTMLDivElement>('#side_bar_button_create')!;
const sideButtonTable = document.querySelector<HTMLDivElement>('#side_bar_button_table')!;

const introPageContent = document.querySelector<HTMLDivElement>('.intro_page')!;
const characterCreationContent = document.querySelector<HTMLDivElement>('.character_creation')!;
const characterOverviewContent = document.querySelector<HTMLDivElement>('.character_overview')!;
const characterViewContent = document.querySelector<HTMLDivElement>('.character_view')!;

const buttonHighlight = "#6786a5";

export function sideBar() {
    sideButtonIntro.addEventListener("click", (showIntroPage));
    sideButtonCreate.addEventListener("click", showCharacterCreation);
    sideButtonTable.addEventListener("click", () => {
        showCharacterOverview();
        generateCharacterTable();
    });
}

export function showIntroPage() {
    characterCreationContent.style.display= "none";
    characterOverviewContent.style.display = "none";
    characterViewContent.style.display = "none";
    introPageContent.style.display = "flex";

    sideButtonIntro.style.backgroundColor = buttonHighlight;
    sideButtonCreate.style.backgroundColor = "";  
    sideButtonTable.style.backgroundColor = "";
    
    createCharacterButton.onclick = (showCharacterCreation);
};

export function showCharacterCreation() {
    introPageContent.style.display = "none";
    characterOverviewContent.style.display = "none";
    characterViewContent.style.display = "none";
    characterCreationContent.style.display = "flex";

    sideButtonCreate.style.backgroundColor = buttonHighlight;
    sideButtonIntro.style.backgroundColor = "";
    sideButtonTable.style.backgroundColor = "";

    adjustSubraceOptions();
    addCharacterToArray();
};

export function showCharacterOverview() {
    introPageContent.style.display = "none";
    characterCreationContent.style.display = "none";
    characterViewContent.style.display = "none";
    characterOverviewContent.style.display = "flex";

    sideButtonTable.style.backgroundColor = buttonHighlight;
    sideButtonIntro.style.backgroundColor = "";
    sideButtonCreate.style.backgroundColor = "";

    createAnotherCharacterButton.onclick = (showCharacterCreation);
};

export function showCharacterView() {
    introPageContent.style.display = "none";
    characterCreationContent.style.display = "none";
    characterOverviewContent.style.display = "none";
    characterViewContent.style.display = "flex";

    exportCharacterJSON();

    backToTableButton.onclick = () => {
        showCharacterOverview();
        generateCharacterTable();
    };
};