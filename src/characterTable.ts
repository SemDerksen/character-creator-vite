// This generally governs how the table is populated with character data

import { showCharacterView  } from "./contentState"

export function generateCharacterTable() {
    let characterData = JSON.parse(localStorage.getItem("characterData")!);
    const table = document.querySelector<HTMLTableElement>("#character_table")!;
    table.innerHTML = "<th>Name</th><th>Race</th><th>Subrace</th><th>Continent</th>";

    // Iterates through array and fills table rows with filtered values of character objects
    for (let i = 0; i < characterData.length; ++i) {
        let characterObject = characterData[i];
        let objectValues = Object.values(characterObject);
        let objectValuesFiltered = [objectValues[0], objectValues[1], objectValues[2], objectValues[5]];
        let row = document.createElement('tr');

        let objectRow = (text: unknown) => {
            let cell = document.createElement('td');
            cell.appendChild(document.createTextNode(<string>text));
            row.appendChild(cell);
        }
        Object.values(objectValuesFiltered).forEach(objectRow);
        table.appendChild(row);

        let button = document.createElement("button");
        button.innerHTML = "View";
        button.value = i.toString();
        button.className = "table_button";
        button.type = "button";
        row.appendChild(button);

        // Switches page content to character view and fills in form with respective character object
        button.onclick = () => {        
            showCharacterView();
            characterData = JSON.parse(localStorage.getItem("characterData")!);

            let selectedCharacterObject = Object.values(characterData[Number(button.value)]);
    
            document.querySelector<HTMLFormElement>("#view_name")!.value = selectedCharacterObject[0];
            document.querySelector<HTMLFormElement>("#view_race")!.value = selectedCharacterObject[1];
            document.querySelector<HTMLFormElement>("#view_subrace")!.value = selectedCharacterObject[2];
            document.querySelector<HTMLFormElement>("#view_gender")!.value = selectedCharacterObject[3];
            document.querySelector<HTMLFormElement>("#view_age")!.value = selectedCharacterObject[4];
            document.querySelector<HTMLFormElement>("#view_continent")!.value = selectedCharacterObject[5];
            document.querySelector<HTMLFormElement>("#view_description")!.value = selectedCharacterObject[6];            
        };
        
        // Appends export button that exports character object to .sjon
        let exportButton = document.createElement("button");
        exportButton.innerHTML = "▼";
        exportButton.value =  i.toString();
        exportButton.className = "export_button";
        exportButton.type = "button";
        row.appendChild(exportButton);

        exportButton.onclick = () => {
            let selectedCharacterObject = characterData[Number(button.value)];
            let selectedCharacterObjectValues = Object.values(characterData[Number(button.value)]);

            let dataStr = JSON.stringify(selectedCharacterObject);
            let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
        
            let exportFileDefaultName = selectedCharacterObjectValues[0] + ".json";
        
            let linkElement = document.createElement('a');
            linkElement.setAttribute('href', dataUri);
            linkElement.setAttribute('download', exportFileDefaultName);
            linkElement.click();
        };

        // Clears characterArray and local storage characterData
        const clearStorageButton = document.querySelector<HTMLFormElement>('#clear_local_storage_button')!;
        
        clearStorageButton.onclick = () => {
            let confirmCharacterDeletion = confirm("Are you certain that you want to delete all character data?");
            if (confirmCharacterDeletion) {
            let characterArray: { [k: string]: FormDataEntryValue; }[] = [];
            localStorage.removeItem("characterData");
            localStorage.setItem("characterData", JSON.stringify(characterArray));
        
            generateCharacterTable();
            }
        };
    }
}


