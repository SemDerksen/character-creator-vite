// This allows the user to export the character object created from form entries to a .json file

export function exportCharacterJSON() {
    let exportCharacterButton = document.querySelector<HTMLButtonElement>("#export_character_button")!;

    exportCharacterButton.onclick = () => {   
        const characterViewForm = document.querySelector<HTMLFormElement>('#character_view_form')!;
        const characterViewFormData = new FormData(characterViewForm);
        const characterViewFormDataObject = Object.fromEntries(characterViewFormData.entries());

        let dataStr = JSON.stringify(characterViewFormDataObject);
        let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);

        let exportFileDefaultName = document.querySelector<HTMLFormElement>("#view_name")!.value + ".json";

        let linkElement = document.createElement('a');
        linkElement.setAttribute('href', dataUri);
        linkElement.setAttribute('download', exportFileDefaultName);
        linkElement.click();
    };
}