// Take inputs and selections from form and create object, then put object in array and save array to local storage

import { showCharacterOverview  } from "./contentState"
import { generateCharacterTable  } from "./characterTable"

const navCharacterTableButton = document.querySelector<HTMLButtonElement>('#nav_character_table_button')!;
const characterForm = document.querySelector<HTMLFormElement>('#character_form')!;

// Creates empty characterArray, then checks whether characterData in local storage returns as null or not, if not it retrieves the data first
// Then it takes all entries from the form and creates an object with key/value pairs, which then gets added to characterArray
// Lastly, it stringifies the array and saves it in local storage under characterData
export function addCharacterToArray() {
  let characterArray: { [k: string]: FormDataEntryValue; }[] = [];
  let characterData = localStorage.getItem("characterData");

  if (characterData != null ) {
    characterArray = JSON.parse(characterData);
  }

  navCharacterTableButton.onclick = () => {
    const characterFormData = new FormData(characterForm);
    const characterFormDataObject = Object.fromEntries(characterFormData.entries());

    characterArray.push(characterFormDataObject);
    localStorage.setItem("characterData", JSON.stringify(characterArray));

    showCharacterOverview();
    generateCharacterTable();
    characterForm.reset();
  };
}

// https://dev.to/deciduously/formdata-in-typescript-24cl

