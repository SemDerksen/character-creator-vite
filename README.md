# Character Creator Vite
This is a project initially created for the Passion Project of Fontys ICT & Media Design M3 Fall 2021-2022. The initial idea comes from a spreadsheet that had a template for character creation of a fictional setting and displayed the characters in a list. This project aims to create a browser-based version with equivalent functionality, allowing someone to quickly create characters and keep them in one place.

## Installation
This project has been set-up with Vite: https://vitejs.dev/.

Use `nvm`

- [NVM For Windows](https://github.com/coreybutler/nvm-windows)
- [NVM for Linux / Mac](https://github.com/nvm-sh/nvm)

And use Node 16.13.1

```bash
nvm install 16.13.1
nvm use 16.13.1

# confirm your node version
node -v
```

Install the dependencies:

```bash
npm install
```

Run the project

```bash
npm run dev
```

Then view the project on http://localhost:3000